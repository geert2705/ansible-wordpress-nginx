## WordPress+Nginx+PHP-FPM Deployment

- Requires Ansible 1.2 or newer
- Expects Debian/Ubuntu hosts

These playbooks deploy a simple all-in-one configuration of the popular
WordPress blogging platform and CMS, frontend by the Nginx web server and the
PHP-FPM process manager. To use, edit `hosts`  inventory file to include the names or 
URLs of the servers you want to deploy, and the group_vars file `all`.

Then run the playbook, like this:

	ansible-playbook -i hosts install.yml

